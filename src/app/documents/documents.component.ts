import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {
  docList = [
    {
      id: 1,
      title: 'abcd doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'pdf'
    },
    {
      id: 2,
      title: 'stunntech doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 3,
      title: 'medical doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 4,
      title: 'patient doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'pdf'
    },
    {
      id: 5,
      title: 'xenai doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 6,
      title: 'abcd doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 7,
      title: 'segal doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'pdf'
    },
    {
      id: 8,
      title: 'runway docPad_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 9,
      title: 'abcd doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 10,
      title: 'abcd doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 11,
      title: 'mithal doc_3',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    },
    {
      id: 12,
      title: 'abcd doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'pdf'
    },
    {
      id: 13,
      title: 'kilwat doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'pdf'
    },
    {
      id: 14,
      title: 'abcd doc_1',
      meta: '0.502',
      summary: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
      thumb: '../../assets/images/doc_thumb.png',
      type: 'jpeg'
    }
  ]
  searchTypes=[
    {
      title: 'All',
      value: 'all'
    },
    {
      title: 'JPEG',
      value: 'jpeg'
    },
    {
      title: 'PDF',
      value: 'pdf'
    }
  ]
  filteredDocList = []
  activeDoc: any = {}
  searchType=this.searchTypes[0].value
  searchText=""

  constructor() { }

  ngOnInit() {
    this.filteredDocList = this.docList;
  }

  onSearchText(text) {
    if(text.trim().length > 0)
      this.filteredDocList = this.docList.filter((item) => item.title.toLowerCase().includes(text.trim().toLowerCase())).filter((item) => item.type === this.searchType)
    else
      this.onSearchType(this.searchType)
  }

  onSearchType(type) {
    if(type !== 'all')
      this.filteredDocList = this.docList.filter((item) => item.type === type).filter((item) => item.title.toLowerCase().includes(this.searchText.trim().toLowerCase()))
    else
      this.filteredDocList = this.docList.filter((item) => item.title.toLowerCase().includes(this.searchText.trim().toLowerCase()))
  }

  onSelectDoc(doc) {
    if(doc)
      this.activeDoc = doc;
  }
}
