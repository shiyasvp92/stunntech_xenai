import { Component, OnInit } from '@angular/core';

const dashboardContents = [
  {
    sectionTitle: 'Document Management System',
    items: [
      {
        title: 'Document360.AI',
        icon: '../../../assets/images/item-icon-02.png',
        color: 'blue',
        url: '/documents'
      }
    ]
  },
  {
    sectionTitle: 'Health Care & Medical',
    items: [
      {
        title: 'Medical360.AI',
        icon: '../../../assets/images/item-icon-01.png',
        color: 'greenBlue',
        url: '/medicalTranscriptor'
      },
      {
        title: 'Patient360.AI',
        icon: '../../../assets/images/item-icon-02.png',
        color: 'pinkRed',
        url: '/patient'
      }
    ]
  },
  {
    sectionTitle: 'Infrastructure & Smart City',
    items: [
      {
        title: 'Utility360.AI',
        icon: '../../../assets/images/item-icon-01.png',
        color: 'green',
        url: '/medical'
      },
      {
        title: 'ANPR360.AI',
        icon: '../../../assets/images/item-icon-02.png',
        color: 'blue',
        url: '/patient'
      },
      {
        title: 'IOT360.AI',
        icon: '../../../assets/images/item-icon-02.png',
        color: 'violet',
        url: '/patient'
      }
    ]
  }
]

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dashItems = dashboardContents;

  constructor() { }

  ngOnInit() {
  }

}
