import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  passwordType = 'password';

  constructor() { }

  ngOnInit() {
  }

  togglePassType() {
    this.passwordType = (this.passwordType ==='text') ? 'password' : 'text';
  }

}
