import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalTranscriptionComponent } from './medical-transcription.component';

describe('MedicalTranscriptionComponent', () => {
  let component: MedicalTranscriptionComponent;
  let fixture: ComponentFixture<MedicalTranscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalTranscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalTranscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
