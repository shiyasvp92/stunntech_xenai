import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medical-transcription',
  templateUrl: './medical-transcription.component.html',
  styleUrls: ['./medical-transcription.component.scss']
})
export class MedicalTranscriptionComponent implements OnInit {
  doctorModels = [
    {
      doctor: 'Dr Akansha   (Neurologist)',
      value: 'akansha'
    },
    {
      doctor: 'Dr Anshidha   (Dermatologist)',
      value: 'anshida'
    },
    {
      doctor: 'Dr  Rosemarry  (Phisiotherapist)',
      value: 'rosemary'
    },
    {
      doctor: 'Dr Raichel   (Phychologist)',
      value: 'raichel'
    },
    {
      doctor: 'Dr Anshidha   (Dermatologist)',
      value: 'dermo'
    }
  ]
  selectedDoc: any = {}
  inputFile: any = {}
  transcriptionMode = "upload"
  resultMsg = "Result is ready"
  resultText = ""
  filesaved = false
  transcribing = true
  modelTraining = false

  constructor() { }

  onDocSelect(doctor) {
    this.selectedDoc = doctor;
  }

  onInputFile(e) {
    this.inputFile = e.target.files[0];
  }

  ngOnInit() {
  }

  onReset() {
    this.selectedDoc = {}
    this.inputFile = {}
    this.transcriptionMode = "upload"
    this.resultMsg = "Result is ready"
    this.resultText = ""
    this.filesaved = false
  }

  onTranscribe() {
    if (this.inputFile.name) {
      this.transcriptionMode = "uploaded";
      this.resultText = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,`
      this.resultMsg = 'Audio is being transcribed'
      this.transcribing = true

      setTimeout(() => {
        this.resultMsg = 'Result is ready'
        this.transcribing = false
      }, 4000)

    }
  }

  onSaveFile() {
    this.resultMsg = 'Saved Successfully'
    this.filesaved = true;
  }

  onTrainModel() {
    this.resultMsg = "Model is being trained.."
    this.modelTraining = true

    setTimeout(() => {
      this.resultMsg = 'Training finished'
      this.modelTraining = false
    }, 4000)
  }
}
